<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/employees_departments', 'WelcomeController@employeesDepartments');

Route::resource('/departments', 'DepartmentResource', ['only' => [
    'index', 'store', 'destroy', 'update', 'edit'
]]);

Route::resource('/employees', 'EmployeeResource', ['only' => [
    'index', 'store', 'destroy', 'update', 'edit'
]]);
