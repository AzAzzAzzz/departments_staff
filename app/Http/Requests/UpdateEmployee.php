<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:25',
            'surname' => 'required|min:3|max:25',
            'patronymic' => 'sometimes|min:3|max:25',
            'sex' => 'numeric',
            'wage' => 'sometimes|numeric',
            'department_id' => 'required|array',
        ];
    }
}
