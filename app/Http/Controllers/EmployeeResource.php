<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateEmployee;
use App\Models\Employee;
use App\Http\Requests\StoreEmployee;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Cache;


class EmployeeResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->response['responseData'] = Cache::remember('employee', 5, function ()
        {
            return Employee::with('departments')->get();
        });
        return response()->json($this->response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEmployee $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        // New Employee Model
        $employee = new Employee();
        $employee->name = $request->get('name');
        $employee->surname = $request->get('surname');
        $employee->patronymic = $request->get('patronymic');
        $employee->sex = $request->get('sex');
        $employee->wage = $request->get('wage');
        $departmentIds = $request->get('department_id');

        // Start Transaction
        DB::beginTransaction();

        if ( ! $employee->save()) {
            DB::rollback();
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('employee.store_failed');
            return response()->json($this->response);
        }

        // Attach an employee to Departments
        $employee->departments()->attach($departmentIds);

        DB::commit();

        // TODO: Remove in Production
        Cache::forget('employee');

        $this->response['responseMessage'] = Lang::get('employee.store_success');
        return response()->json($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::with('departments')->find($id);

        if ( ! $employee) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('employee.show_failed');
            return response()->json($this->response);
        }

        $this->response['responseData'] = $employee;
        return response()->json($this->response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEmployee $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployee $request, $id)
    {
        // Get an Employee and related Departments
        $employee = Employee::with('departments')->find($id);

        // Set New values ​​for fields
        $employee->name = $request->get('name');
        $employee->surname = $request->get('surname');
        $employee->patronymic = $request->get('patronymic');
        $employee->sex = $request->get('sex');
        $employee->wage = $request->get('wage');
        $departmentIds = $request->get('department_id');

        // Save Employee data
        if ( !  $employee->save()) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('employee.update_failed');
            return response()->json($this->response);
        }

        // Synchronization of relations
        $employee->departments()->sync($departmentIds);

        // TODO: Remove in Production
        Cache::flush();

        $this->response['responseMessage'] = Lang::get('employee.update_success');
        return response()->json($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get Employee Model
        $employee = Employee::with('departments')->find($id);

        // Detach Departments
        $detach = $employee->departments()->detach($employee->departments);

        if ( ! $detach) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('employee.destroy_failed');
            return response()->json($this->response);
        }

        // Remove Employee Data
        if ( ! $employee->destroy($id)) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('employee.destroy_failed');
            return response()->json($this->response);
        }

        // TODO: Remove in Production
        Cache::flush();

        $this->response['responseMessage'] = Lang::get('employee.destroy_success');
        return response()->json($this->response);
    }
}
