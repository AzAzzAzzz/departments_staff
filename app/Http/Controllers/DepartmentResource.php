<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Http\Requests\StoreDepartment;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;

class DepartmentResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Department from the cache, if it does not exist, extract from database
        $this->response['responseData'] = Cache::remember('department', 5, function ()
        {
            return Department::with('employees')->get();
        });
        return response()->json($this->response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDepartment $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreDepartment $request)
    {
        $department = new Department();
        $department->name = $request->get('name');

        // Save Department Model
        if ( ! $department->save()) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('department.store_failed');
            return response()->json($this->response);
        }

        // TODO: Remove in Production
        Cache::forget('department');

        $this->response['responseMessage'] = Lang::get('department.store_success');
        return response()->json($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);

        if ( ! $department) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('department.show_failed');
            return response()->json($this->response);
        }

        $this->response['responseData'] = $department;
        return response()->json($this->response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);
        $department->name= $request->get('name');

        if ( ! $department->save()) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('department.update_error');
            return response()->json($this->response);
        }

        // TODO: Remove in Production
        Cache::flush();

        $this->response['responseMessage'] = Lang::get('department.update_success');
        return response()->json($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( ! Department::destroy($id)) {
            $this->response['error'] = true;
            $this->response['responseMessage'] = Lang::get('department.destroy_failed');
            return response()->json($this->response);
        }

        // TODO: Remove in Production
        Cache::flush();

        $this->response['responseMessage'] = Lang::get('department.destroy_success');
        return response()->json($this->response);
    }
}
