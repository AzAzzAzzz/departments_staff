<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\Cache;

class WelcomeController extends Controller
{
    /**
     * Return Employees Departments
     * @return \Illuminate\Http\JsonResponse
     */
    public function employeesDepartments ()
    {
        // Get Employees and Departments in cache, if it exist
        if (Cache::has('employees_departments')) {
            $this->response['responseData'] = Cache::get('employees_departments');
            return response()->json($this->response);
        }

        // Get Employees and Departments from database
        $departments = [];
        $employeeDepartment = [];
        $employeesDepartments['employees'] = Employee::with('departments')
            ->get()
            ->each(function ($employee) use (&$departments, &$employeeDepartment) {
                // Get All Departments from Employees
                foreach ($employee->departments as $department) {
                    if (empty($departments[$department->id])) {
                        $departments[$department->id] = $department;
                    }
                    $employeeDepartment[$employee->id][$department->id] = $department;
                }
            });
        $employeesDepartments['departments'] = $departments;
        $employeesDepartments['employees_department'] = $employeeDepartment;

        // Write an Employees and Departments, if it does not exist
        Cache::add('employees_departments', $employeesDepartments, 5);

        $this->response['responseData'] = $employeesDepartments;
        return response()->json($this->response);
    }
}
