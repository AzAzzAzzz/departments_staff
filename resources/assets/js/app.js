
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('app-navbar', require('./components/Navbar'));

import VueRouter from 'vue-router'

const Welcome = require('./components/Welcome');
const Departments = require('./components/Departments');
const DepartmentCreate = require('./components/DepartmentCreate');
const DepartmentEdit = require('./components/DepartmentEdit');
const Employees = require('./components/Employees');
const EmployeesCreate = require('./components/EmployeesCreate');
const EmployeeEdit = require('./components/EmployeeEdit');

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Welcome, name: 'welcome' },
        { path: '/departments', component: Departments, name: 'departments' },
        { path: '/department-create', component: DepartmentCreate, name: 'departmentCreate' },
        { path: '/department/:id/edit', component: DepartmentEdit, name: 'DepartmentEdit' },
        { path: '/employees', component: Employees, name: 'employees' },
        { path: '/employee-create', component: EmployeesCreate, name: 'employeeCreate' },
        { path: '/employee/:id/edit', component: EmployeeEdit, name: 'employeeEdit' },
    ]
});

const app = new Vue({
    el: '#app',
    router: router
});
