<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = new Employee();
        $employee->name = 'Иван';
        $employee->surname = 'Йода';
        $employee->sex = 1;
        $employee->wage = 45000;
        $employee->save();

        $employee->departments()->attach([2]);

        $employee = new Employee();
        $employee->name = 'Петр';
        $employee->surname = 'Вейдер';
        $employee->sex = 1;
        $employee->wage = 32000;
        $employee->save();

        $employee->departments()->attach([1]);

        $employee = new Employee();
        $employee->name = 'Ольга';
        $employee->surname = 'Кеноби';
        $employee->sex = 2;
        $employee->wage = 37000;
        $employee->save();

        $employee->departments()->attach([2,3]);
    }
}
