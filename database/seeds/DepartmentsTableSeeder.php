<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            ['name' => 'Отдел закупок', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Отдел продаж', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'PR-отдел', 'created_at' => date('Y-m-d H:i:s')],
        ]);
    }
}
