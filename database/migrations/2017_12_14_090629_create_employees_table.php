<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 25)->comment('Имя сотрудника');
            $table->string('surname', 25)->comment('Фамилия');
            $table->string('patronymic', 25)->nullable()->comment('Отчество');
            $table->tinyInteger('sex')->default(0)->comment('Пол сотрудника 0 - Не указан, 1 - Муж, 2 - Жен');
            $table->integer('wage')->nullable()->unsigned()->comment('Заработная плата сотрудника');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
